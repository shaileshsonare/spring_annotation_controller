<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bye World!</h1>
        
        ${show_employee_name}
        
        <br/>
        
        <c:forEach items="${result}" var="test">
            ${test} <br/>
        </c:forEach>
            
        <c:forEach items="${map}" var="test">
            ${test.key} = ${test.value} <br/>
        </c:forEach>
    </body>
</html>
