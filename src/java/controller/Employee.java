
package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employee")
public class Employee {
    
    @RequestMapping("/show_employee")
    public ModelAndView showEmployeeData(@RequestParam("name") String employee_name){
        
        ModelAndView model_view = new ModelAndView("employee/show_employee");
        // mapped with /WEB-INF/jsp/employee/show_employee.jsp
        
        List list = new ArrayList();
        list.add(0, "Sunday");
        list.add(1, "Monday");
        list.add(2, "Tuesday");
        list.add(3, "Wednesday");
        list.add(4, "Thursday");
        list.add(5, "Friday");
        list.add(6, "Saturday");
        
        Map<String,String> map = new HashMap<>();
        map.put("abc", "123");
        map.put("xyz", "456");
        
        model_view.addObject("show_employee_name", employee_name);
        model_view.addObject("result", list);
        model_view.addObject("map", map);
        
        return model_view;
    }
    
    @RequestMapping("/show_employee_data")
    public ModelAndView showEmployee(){
        ModelAndView model_view = new ModelAndView("employee/show_data");
        return model_view;
    }
    
}
